﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Using_Interface temp = new Using_Interface();
            if(temp is Using_Interface)
            {
                Debug.WriteLine("yes its right");
            }

            Using_Interface_2 temp2 = new Using_Interface_2();
            temp2.hello_there();
            

            Learning_Interface temp3 = temp;
            temp3.hello_there();

        }
    }

    public interface Learning_Interface
    {
        void hello_there();
    }

    class Using_Interface : Learning_Interface
    {
        void Learning_Interface.hello_there()
        {
            Debug.WriteLine("Using_Interface - Learning_Interface.hello_there");
        }
        /*

        public void hello_there()
        {
            Debug.WriteLine("hello_there");
        }
        */
    }

    class Using_Interface_2 : Learning_Interface
    {
        public void hello_there()
        {
            Debug.WriteLine("Using_Interface_2 - hello_there");
        }
    }
}
